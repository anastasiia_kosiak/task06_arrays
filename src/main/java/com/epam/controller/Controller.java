package com.epam.controller;

import com.epam.model.*;
import com.epam.model.Deque;
import com.epam.view.MenuView;
import com.epam.view.Task;


import java.io.IOException;
import java.util.*;
import java.util.PriorityQueue;

public class Controller {

    private FunctionalityTest model;
    private LogicTask arrayModel;
    private MenuView view;
    private Game game;

    private Task comparePerformance;
    private Task showComparisons;
    private Task findElement;
    private Task testDeque;
    private Task testPriorityQueue;
    private Task testArrays;
    private Task showGameResults;
    private Task quit;


    public void startMenu() {
        view.showMenu();
        view.awaitValidMenuOption();
    }
    public Controller() {
        model = new FunctionalityTest();
        arrayModel = new LogicTask();
        view = new MenuView();
        game = new Game();

        comparePerformance = new Task() {
            @Override
            public void doTask() {
                int size = 2000;
                long customMethodTime = model.measurePerformance(new StringArray(), size);
                long builtinMethodTime = model.measurePerformance(new ArrayList<String>(), size);

                view.printMessage("Time taken to add " + size
                        + " Strings to StringArray: " + customMethodTime);

                view.printMessage("Time taken to add " + size
                        + " Strings to ArrayList<String>: " + builtinMethodTime);
            }
        };
        view.addMenuItem("1", comparePerformance);
        showComparisons = new Task() {
            @Override
            public void doTask() {
                int size = 20;

                try {
                    PairedString[] stringPairs = model.generatePairs(size);
                    view.print(Arrays.toString(stringPairs));

                    Arrays.sort(stringPairs);
                    view.print(Arrays.toString(stringPairs));

                    ArrayList<PairedString> pairsOfStrings =
                            randomise(stringPairs);
                    view.print(pairsOfStrings);

                    pairsOfStrings.sort(new PairedStringComparator());
                    view.print(pairsOfStrings);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        view.addMenuItem("2", showComparisons);

        findElement = new Task() {
            @Override
            public void doTask() {
                int size = 20;
                ArrayList<PairedString> list = new ArrayList<>();

                try {
                    PairedString[] array = model.generatePairs(size);
                    list = new ArrayList<PairedString>(
                            Arrays.asList(array)
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }

                list.sort(new PairedStringComparator());
                view.print(list);
                String query = view.requestSearchQuery();
                PairedString pair = new PairedString(query);
                int found = Collections.binarySearch(list, pair,
                        new PairedStringComparator());
                view.print(found);
            }
        };
        view.addMenuItem("3", findElement);

        testDeque = new Task() {

            @Override
            public void doTask() {
                Deque deque = new Deque<Integer>(4);

                deque.offerFirst(1);
                view.print(deque.peekFirst().toString());
                deque.offerLast(2);
                view.print(deque.peekLast().toString());
                deque.offerLast(3);
                view.print(deque.peekLast().toString());
                deque.pollFirst();
                view.print(deque.peekFirst().toString());
                deque.pollLast();
                view.print(deque.peekLast().toString());
            }
        };
        view.addMenuItem("4", testDeque);
        testPriorityQueue = new Task() {
            @Override
            public void doTask() {
                PriorityQueue priorityQueue = new PriorityQueue();

                priorityQueue.offer(1);
                priorityQueue.offer(2);
                view.print(priorityQueue.peek().toString());
                priorityQueue.poll();
                view.print(priorityQueue.poll().toString());
            }
        };
        view.addMenuItem("5", testPriorityQueue);

        testArrays = new Task() {
            @Override
            public void doTask() {
                Object[] one = { 2, 3, 5, 1, 4};
                view.printMessage("First array: ");
                view.print(Arrays.toString(one));

                Object[] two = { 2, 6, 5, 7, 4};
                view.printMessage("Second array: ");
                view.print(Arrays.toString(two));

                Object[] three = arrayModel.getElementsFromBothArrays(one, two);
                view.printMessage("Elements occurring in both arrays: ");
                view.print(Arrays.toString(three));

                Object[] four = arrayModel.getElementsFromOneArray(one, two);
                view.printMessage("Elements occurring in one array: ");
                view.print(Arrays.toString(four));

                Object[] array = { 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 };
                view.printMessage("Array with duplicates: ");
                view.print(Arrays.toString(array));

                HashMap<Object, Integer> occurrences = arrayModel.countOccurrences(array);
                Object[] filtered = arrayModel.removeDuplicates(array, occurrences, 2);
                view.printMessage("Array with elements occurring more than two times removed: ");
                view.print(Arrays.toString(filtered));

                Object[] input = { 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 1, 2, 2 };
                view.printMessage("Array with consecutive duplicates: ");
                view.print(Arrays.toString(input));

                Object[] output = arrayModel.removeConsecutive(input);
                view.printMessage("Array with consecutive duplicates removed: ");
                view.print(Arrays.toString(output));
            }
        };
        view.addMenuItem("6", testArrays);

        showGameResults = new Task() {
            @Override
            public void doTask() {
                int[] doors = game.generateRandomArray(10,
                        -100, 80,
                        -5, 10);
                view.printTable(doors);
                boolean canSurvive = game.calculateSumOfElements(doors) >= 0;
                if (canSurvive) {
                    int[] openDoorsOrder = game.separateIndices(doors);
                    view.printMessage("A character can survive if they open" +
                            " the doors in the following order: ");
                    view.print(Arrays.toString(openDoorsOrder));
                } else {
                    view.printMessage("A character can't survive.");
                }
            }
        };
        view.addMenuItem("7", showGameResults);

        quit = new Task() {
            @Override
            public void doTask() {
                System.exit(0);
            }
        };
        view.addMenuItem("q", quit);
    }

    private <T> ArrayList<T> randomise(T[] array) {
        ArrayList<T> list = new ArrayList<>();
        Random rand = new Random();

        for (int i = 0; i < array.length; i++) {
            int randomIndex;
            do {
                randomIndex = rand.nextInt(array.length);
            } while (array[randomIndex] == null);

            list.add(array[randomIndex]);
            array[randomIndex] = null;
        }
        return list;
    }
}