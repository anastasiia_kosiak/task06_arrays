package com.epam.view;

import java.io.Serializable;
import java.util.TreeMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    protected Scanner input;
    protected TreeMap<String, Item> menu;

    public View() {
        input = new Scanner(System.in, "UTF-8");
        menu = new TreeMap<>();

        Item quit = new Item("q", "Quit");
        menu.put("q", quit);
    }

    public void showMenu() {
        System.out.println("MENU");

        for (Map.Entry<String, Item> option : menu.entrySet()) {
            System.out.printf("%s - %s%n", option.getValue().getKey(),
                    option.getValue().getDescription());
        }
    }

    public void awaitValidMenuOption() {
        String choice;
        do {
            choice = input.nextLine().toLowerCase();
        } while (!menu.keySet().contains(choice));

        menu.get(choice).getAction().doTask();
    }

    public void addMenuItem(String key, Task action) {
        Item item = menu.get(key);
        item.setAction(action);
        menu.put(key, item);
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public void print(Serializable contents) {
        System.out.println(contents.toString());
    }
}
