package com.epam.view;

public class MenuView extends View{
    public MenuView() {
        super();
        Item comparePerformance = new Item("1", "StringArray vs ArrayList");
        super.menu.put("1", comparePerformance);

        Item showComparisons = new Item("2", "Sort Paired Strings");
        super.menu.put("2", showComparisons);

        Item findElement = new Item("3", "Find an element via binary search");
        super.menu.put("3", findElement);

        Item testDeque = new Item("4", "See custom deque in use");
        super.menu.put("4",testDeque);

        Item testPriorityQueue = new Item("5", "See custom priority queue in use");
        super.menu.put("5", testPriorityQueue);

       Item testArrays = new Item("6", "See array manipulations");
        super.menu.put("6", testArrays);

        Item showGameResults = new Item("7", "See game statistics");
        super.menu.put("7", showGameResults);
    }

    public String requestSearchQuery() {
        String query = "";

        while (query.isEmpty()) {
            System.out.println("Search for (e.g. \"Belgium — Brussels\"): ");
            query = input.nextLine();
        }

        return query;
    }

    public void displayFound(String found) {
        System.out.print("Found: ");
        System.out.println(found);
    }

    public void printTable(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%2d: %2d%n", i, array[i]);
        }
    }
}

