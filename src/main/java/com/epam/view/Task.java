package com.epam.view;

@FunctionalInterface
public interface Task {
    void doTask();
}
