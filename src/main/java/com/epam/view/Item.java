package com.epam.view;



public class Item {
    private String key;
    private String description;
    private Task action;

    Item(String key, String description) {
        this.key = key;
        this.description = description;
    }



    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public Task getAction() {
        return action;
    }

    public void setAction(Task action) {
        this.action = action;
    }
}

