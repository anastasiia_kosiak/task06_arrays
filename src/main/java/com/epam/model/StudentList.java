package com.epam.model;

public class StudentList<T> {
    private Student[] list;
    private int size;
    private int lastIndex;

    public StudentList() {
        list = new Student[size];
        size = 2;
        lastIndex = 0;
    }

    public void add(T item) throws Exception {
        if (lastIndex > size) {
            throw new Exception("Index is out of boundaries");
        }
        if (lastIndex == size) {
            size *= 2;
            list = new Student[size];
        }
        list[lastIndex] = (Student) item;
        ++lastIndex;
    }
}
