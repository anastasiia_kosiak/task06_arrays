package com.epam.model;
import java.io.Serializable;

public class PairedString implements Comparable<PairedString>, Serializable{
    private String firstString;
    private String secondString;

    public PairedString(String firstString, String secondString) {
        this.firstString = firstString;
        this.secondString = secondString;
    }

    public PairedString(String dashedString) {
        String[] split = dashedString.split("—");
        firstString = split[0].trim();
        secondString = split[1].trim();
    }
    @Override
    public String toString() {
        return firstString + " — " + secondString;
    }

    @Override
    public int compareTo(PairedString str) {
        return firstString.compareTo(str.firstString);
    }
    public String getTwo() {
        return this.secondString;
    }
}
