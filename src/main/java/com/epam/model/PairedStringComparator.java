package com.epam.model;
import java.util.Comparator;

public  class PairedStringComparator implements Comparator<PairedString>{
    @Override
    public int compare(PairedString first, PairedString second) {
        return first.getTwo().compareTo(second.getTwo());
    }
}
