package com.epam.model;

import java.util.Arrays;
import java.util.HashMap;

public class LogicTask {

    public Object[] getElementsFromBothArrays(Object[] arr1, Object[] arr2) {
        int size = Math.max(arr1.length, arr2.length);
        Object[] arr3 = new Object[size];
        int nextIndex = 0;

        for (int i = 0; i < arr1.length; ++i) {
            if (isInArray(arr2, arr1[i])) {
                arr3[nextIndex] = arr1[i];
                ++nextIndex;
            }
        }
        size = nextIndex;
        Object[] arr4 = Arrays.copyOfRange(arr3, 0, size);

        return arr4;
    }
    private boolean isInArray(Object[] array, Object element) {
        Object[] copy = array.clone();
        Arrays.sort(copy);
        int index = Arrays.binarySearch(copy, element);
        return index >= 0;
    }

    public Object[] getElementsFromOneArray(Object[] arr1, Object[] arr2) {
        int size = arr1.length + arr2.length;
        Object[] three = new Object[size];
        int nextIndex = 0;

        for (int i = 0; i < arr1.length; ++i) {
            if (!isInArray(arr2, arr1[i])) {
                three[nextIndex] = arr1[i];
                ++nextIndex;
            }
        }
        for (int j = 0; j < arr2.length; ++j) {
            if (!isInArray(arr1, arr2[j])) {
                three[nextIndex] = arr2[j];
                ++nextIndex;
            }
        }
        size = nextIndex;
        Object[] fourth = Arrays.copyOfRange(three, 0, size);
        return fourth;
    }

    public HashMap<Object, Integer> countOccurrences(Object[] array) {

        HashMap<Object, Integer> occurrences = new HashMap<>();

        for (int i = 0; i < array.length; i++) {
            if (occurrences.containsKey(array[i])) {
                Integer oldValue = occurrences.get(array[i]);
                occurrences.replace(array[i], oldValue + 1);
            } else {
                occurrences.put(array[i], 1);
            }
        }

        return occurrences;
    }

    public Object[] removeDuplicates (Object[] array, HashMap<Object, Integer> occurrences,
                                      int numberOfDuplicatesAllowed) {
        Object[] tmp = new Object[array.length];
        int nextIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (occurrences.get(array[i]) <= numberOfDuplicatesAllowed) {
                tmp[nextIndex] = array[i];
                nextIndex++;
            }
        }
        Object[] shortenedArray = Arrays.copyOfRange(tmp, 0, nextIndex);
        return shortenedArray;
    }

    public Object[] removeConsecutive(Object[] array) {
        Object[] tmp = new Object[array.length];
        tmp[0] = array[0];
        int nextIndex = 1;
        Object lastElement = array[0];

        for (int i = 1; i < array.length; ++i) {
            if (!array[i].equals(lastElement)) {
                tmp[nextIndex] = array[i];
                nextIndex++;
                lastElement = array[i];
            }
        }

        Object[] shortenedArray = Arrays.copyOfRange(tmp, 0, nextIndex);
        return shortenedArray;
    }
}
