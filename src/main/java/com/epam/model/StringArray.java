package com.epam.model;

import java.util.AbstractCollection;
import java.util.ArrayList;

public class StringArray extends ArrayList {
    private String[] strings;
    private int size;
    private int nextIndex;

    public StringArray() {
        size = 1;
        strings = new String[size];
        nextIndex = 0;
    }

    public void addString(String s) {
        if (nextIndex > size) {
            throw new RuntimeException("Index is out of bounds");
        }
        if (nextIndex == size) {
            size *= 2;
            strings = new String[size];
        }
        strings[nextIndex] = s;
        ++nextIndex;
    }

    public String getString(int index) {
        if (index >= nextIndex) {
            throw new RuntimeException("Index is out of bounds.");
        }
        return strings[index];
    }
}