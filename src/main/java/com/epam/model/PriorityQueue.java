package com.epam.model;

import java.util.AbstractQueue;
import java.util.HashMap;
import java.util.Iterator;
public class PriorityQueue<T extends Object> extends AbstractQueue {

    private HashMap<Integer, T> queue;
    private int size;
    private int lastIndex;
    private int maxPriority;

    public PriorityQueue() {
        queue = new HashMap<>();
    }
    public Iterator iterator() {
        return null;
    }
    public int size() {
        return size;
    }

    public boolean offer(Object item, int priority) {
        if (queue.containsValue(item)) {
            return false;
        }
        queue.put(priority, (T) item);
        if (priority > maxPriority) {
            maxPriority = priority;
        }
        return true;
    }
    public boolean offer(Object item) {
        if (queue.containsValue(item)) {
            return false;
        }
        queue.put(0, (T) item);
        maxPriority = 0;
        return true;
    }
    public boolean isEmpty() {
        return queue.isEmpty();
    }
    public T poll() {
        T head = queue.get(maxPriority);
        int headPriority = maxPriority;
        maxPriority = 0;
        for(Integer priority : queue.keySet()) {
            if (priority > maxPriority) {
                maxPriority = priority;
            }
        }
        queue.remove(headPriority);
        findMaxPriority();

        return head;
    }
    public Object peek() {
        return queue.get(maxPriority);
    }
    private void findMaxPriority() {
        maxPriority = 0;
        for (Integer key : queue.keySet()) {
            if (key > maxPriority) {
                maxPriority = key;
            }
        }
    }
}
