package com.epam.model;

import java.util.Random;

public class Game {
    public int[] generateRandomArray(int size, int lowest, int highest,
                                     int minNotExcluded, int maxNotExcluded) {
        int[] numbers = new int[size];
        int nextIndex = 0;
        Random generator = new Random();
        int generated;
        for (int i = 0; i < size; ++i) {
            do {
                generated = generator.nextInt(highest - lowest + 1);
            } while (generated + lowest > minNotExcluded
                    && generated + lowest < maxNotExcluded);
            numbers[nextIndex] = generated + lowest;
            nextIndex++;
        }

        return numbers;
    }

    public int calculateSumOfElements(int[] array) {
        int sum = 0;

        for (int element : array) {
            sum += element;
        }
        return sum;
    }

    public int[] separateIndices(int[] array) {
        int[] indices = new int[array.length];
        int nextPostIndex = 0;
        int nextPreIndex = array.length - 1;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                indices[nextPostIndex] = i;
                nextPostIndex++;
            } else {
                indices[nextPreIndex] = i;
                nextPreIndex--;
            }
        }
        return indices;
    }
}
