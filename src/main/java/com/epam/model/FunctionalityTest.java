package com.epam.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.Random;
import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Stream;

public class FunctionalityTest {

    public PairedString[] generatePairs(int number) throws IOException {
        PairedString[] pairs = new PairedString[number];
        Path source = Paths.get("country_capital.txt");
        Random rand = new Random();
        int index;
        try (BufferedReader fileReader = new BufferedReader(new FileReader("country_capital.txt"))) {
            for (int i = 0; i < number; ++i) {
                PairedString newest = new PairedString(fileReader.readLine());
                do {
                    index = rand.nextInt(number);
                } while (pairs[index] != null);
                pairs[index] = newest;
            }
        }
        return pairs;
    }
    public long measurePerformance(ArrayList strings, int size) {
        long start= System.currentTimeMillis();
        for(int i = 0; i <size; ++i) {
            strings.add("new string");
        }
        long end = System.currentTimeMillis();
        return end - start;
    }
}
