package com.epam.model;

import java.util.ArrayList;

public class Deque<T> {
    private static int capacity = 64;
    private ArrayList<T> deque;

    public Deque() {
        deque = new ArrayList<>();
    }

    public Deque(int capacity) {
        this.capacity = capacity;
        deque = new ArrayList<>();
    }

    public boolean offerFirst(T item) {
        if (deque.size() < capacity) {
            deque.add(0, item);
            return true;
        } else {
            return false;
        }
    }

    public boolean offerLast(T item) {
        if (deque.size() < capacity) {
            deque.add(item);
            return true;
        } else {
            return false;
        }
    }

    public T pollFirst() {
        T temp = deque.get(0);
        deque.remove(0);

        return temp;
    }

    public T pollLast() {
        T temp = deque.get(deque.size() - 1);
        deque.remove(deque.size() - 1);

        return temp;
    }

    public T peekFirst() {
        return deque.get(0);
    }

    public T peekLast() {
        return deque.get(deque.size() - 1);
    }
}

